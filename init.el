;; -*- mode: emacs-lisp; lexical-binding: t -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Layer configuration:
This function should only modify configuration layer settings."
  (setq-default
   ;; Base distribution to use. This is a layer contained in the directory
   ;; `+distribution'. For now available distributions are `spacemacs-base'
   ;; or `spacemacs'. (default 'spacemacs)
   dotspacemacs-distribution 'spacemacs

   ;; Lazy installation of layers (i.e. layers are installed only when a file
   ;; with a supported type is opened). Possible values are `all', `unused'
   ;; and `nil'. `unused' will lazy install only unused layers (i.e. layers
   ;; not listed in variable `dotspacemacs-configuration-layers'), `all' will
   ;; lazy install any layer that support lazy installation even the layers
   ;; listed in `dotspacemacs-configuration-layers'. `nil' disable the lazy
   ;; installation feature and you have to explicitly list a layer in the
   ;; variable `dotspacemacs-configuration-layers' to install it.
   ;; (default 'unused)
   dotspacemacs-enable-lazy-installation 'nil

   ;; If non-nil then Spacemacs will ask for confirmation before installing
   ;; a layer lazily. (default t)
   dotspacemacs-ask-for-lazy-installation t

   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (i.e. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()

   ;; List of configuration layers to load.
   dotspacemacs-configuration-layers
   '((c-c++ :variables
            c-c++-default-mode-for-headers 'c++-mode
            c-c++-enable-clang-format-on-save 't)
     csharp
     cmake
     dart
     dash
     emacs-lisp
     git
     gpu
     html
     imenu-list
     ivy
     lsp
     octave
     org
     (python :variables
             python-format-on-save nil
             python-sort-imports-on-save t
             python-lsp-server 'pylsp
             python-formatter 'black)
     rust
     (shell :variables
            shell-default-height 30
            shell-default-position 'bottom
            shell-default-shell 'eshell)
     windows-scripts
     yaml)

   ;; List of additional packages that will be installed without being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages, then consider creating a layer. You can also put the
   ;; configuration in `dotspacemacs/user-config'.
   ;; To use a local version of a package, use the `:location' property:
   ;; '(your-package :location "~/path/to/your-package/")
   ;; Also include the dependencies as they will not be resolved automatically.
   dotspacemacs-additional-packages '(ag
                                      dired-du
                                      ;; (frame-cmds :location (recipe :fetcher github
                                      ;;                               :repo "emacsmirror/frame-cmds"))
				                              gnu-elpa-keyring-update
                                      highlight2clipboard
                                      org-chef
                                      rg
                                      git-auto-commit-mode
                                      golden-ratio-scroll-screen
                                      itail
                                      moom
                                      multiple-cursors
                                      mwim
                                      nhexl-mode
                                      nord-theme
                                      operate-on-number
                                      prettier-js
                                      request
                                      ;; workaround for renamed string-edit package (renamed from string-edit to string-edit-at-point leading to
                                      ;; issues in spacemacs, that expects string-edit. the commit is  the last before the rename of the package)
                                      ;; should be removed after the spacemacs issue is resolved (see https://github.com/syl20bnr/spacemacs/issues/15648)
                                      (string-edit :location (recipe :fetcher github :repo "magnars/string-edit.el" :commit "d7c4b9db6c4987b5c022a9858e6302a4c53aff5f"))
                                      ;; evil-ediff was removed from elpa, but is still referenced in spacemacs-edit layer...
                                      (evil-ediff :location (recipe :fetcher github :repo "emacs-evil/evil-ediff"))
                                      )
   ;; A list of packages that cannot be updated.
   dotspacemacs-frozen-packages '()

   ;; A list of packages that will not be installed and loaded.
   dotspacemacs-excluded-packages '(
                                    ; Until auto-complete-rst supports Python 3,
                                    ; we can't really use it.
                                    auto-complete-rst
                                    orgit
                                    ; Slows down eshell on Windows
                                    eshell-prompt-extras)
   ;; Defines the behaviour of Spacemacs when installing packages.
   ;; Possible values are `used-only', `used-but-keep-unused' and `all'.
   ;; `used-only' installs only explicitly used packages and deletes any unused
   ;; packages as well as their unused dependencies. `used-but-keep-unused'
   ;; installs only the used packages but won't delete unused ones. `all'
   ;; installs *all* packages supported by Spacemacs and never uninstalls them.
   ;; (default is `used-only')
   dotspacemacs-install-packages 'used-only))

(defun dotspacemacs/init ()
  "Initialization:
This function is called at the very beginning of Spacemacs startup,
before layer configuration.
It should only modify the values of Spacemacs settings."
  (defconst windows-p (string-equal system-type "windows-nt"))
  (defconst macos-p (string-equal system-type "darwin"))
  (defconst linux-p (string-equal system-type "gnu/linux"))
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil then enable support for the portable dumper. You'll need
   ;; to compile Emacs 27 from source following the instructions in file
   ;; EXPERIMENTAL.org at to root of the git repository.
   ;; (default nil)
   dotspacemacs-enable-emacs-pdumper nil

   ;; Name of executable file pointing to emacs 27+. This executable must be
   ;; in your PATH.
   ;; (default "emacs")
   dotspacemacs-emacs-pdumper-executable-file "emacs"

   ;; Name of the Spacemacs dump file. This is the file will be created by the
   ;; portable dumper in the cache directory under dumps sub-directory.
   ;; To load it when starting Emacs add the parameter `--dump-file'
   ;; when invoking Emacs 27.1 executable on the command line, for instance:
   ;;   ./emacs --dump-file=$HOME/.emacs.d/.cache/dumps/spacemacs-27.1.pdmp
   ;; (default (format "spacemacs-%s.pdmp" emacs-version))
   dotspacemacs-emacs-dumper-dump-file (format "spacemacs-%s.pdmp" emacs-version)

   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t

   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5

   ;; Set `gc-cons-threshold' and `gc-cons-percentage' when startup finishes.
   ;; This is an advanced option and should not be changed unless you suspect
   ;; performance issues due to garbage collection operations.
   ;; (default '(100000000 0.1))
   dotspacemacs-gc-cons '(100000000 0.1)

   ;; Set `read-process-output-max' when startup finishes.
   ;; This defines how much data is read from a foreign process.
   ;; Setting this >= 1 MB should increase performance for lsp servers
   ;; in emacs 27.
   ;; (default (* 1024 1024))
   dotspacemacs-read-process-output-max (* 1024 1024)

   ;; If non-nil then Spacelpa repository is the primary source to install
   ;; a locked version of packages. If nil then Spacemacs will install the
   ;; latest version of packages from MELPA. Spacelpa is currently in
   ;; experimental state please use only for testing purposes.
   ;; (default nil)
   dotspacemacs-use-spacelpa nil

   ;; If non-nil then verify the signature for downloaded Spacelpa archives.
   ;; (default t)
   dotspacemacs-verify-spacelpa-archives t

   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil

   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default 'emacs-version)
   dotspacemacs-elpa-subdirectory 'emacs-version

   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'emacs

   ;; If non-nil show the version string in the Spacemacs buffer. It will
   ;; appear as (spacemacs version)@(emacs version)
   ;; (default t)
   dotspacemacs-startup-buffer-show-version t

   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner "~/.spacemacs.d/emacs-logo.png"

   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `recents-by-project' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   ;; The exceptional case is `recents-by-project', where list-type must be a
   ;; pair of numbers, e.g. `(recents-by-project . (7 .  5))', where the first
   ;; number is the project limit and the second the limit on the recent files
   ;; within a project.
   dotspacemacs-startup-lists '((recents . 5)
                                (projects . 7))

   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t

   ;; Show numbers before the startup list lines. (default t)
   dotspacemacs-show-startup-list-numbers t

   ;; The minimum delay in seconds between number key presses. (default 0.4)
   dotspacemacs-startup-buffer-multi-digit-delay 0.4

   ;; Default major mode for a new empty buffer. Possible values are mode
   ;; names such as `text-mode'; and `nil' to use Fundamental mode.
   ;; (default `text-mode')
   dotspacemacs-new-empty-buffer-major-mode 'text-mode

   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'org-mode

   ;; If non-nil, *scratch* buffer will be persistent. Things you write down in
   ;; *scratch* buffer will be saved and restored automatically.
   dotspacemacs-scratch-buffer-persistent nil

   ;; If non-nil, `kill-buffer' on *scratch* buffer
   ;; will bury it instead of killing.
   dotspacemacs-scratch-buffer-unkillable nil

   ;; Initial message in the scratch buffer, such as "Welcome to Spacemacs!"
   ;; (default nil)
   dotspacemacs-initial-scratch-message nil

   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes '(
                         sanityinc-tomorrow-day
                         nord
                         ;; leuven
                         ;; spacemacs-light
                         ;; solarized-light
                         ;; solarized-dark
                         ;; leuven
                         ;; spacemacs-dark
                         ;; monokai
                         )

   ;; Set the theme for the Spaceline. Supported themes are `spacemacs',
   ;; `all-the-icons', `custom', `doom', `vim-powerline' and `vanilla'. The
   ;; first three are spaceline themes. `doom' is the doom-emacs mode-line.
   ;; `vanilla' is default Emacs mode-line. `custom' is a user defined themes,
   ;; refer to the DOCUMENTATION.org for more info on how to create your own
   ;; spaceline theme. Value can be a symbol or list with additional properties.
   ;; (default '(spacemacs :separator wave :separator-scale 1.5))
   dotspacemacs-mode-line-theme '(spacemacs :separator wave :separator-scale 1.5)

   ;; If non-nil the cursor color matches the state color in GUI Emacs.
   ;; (default t)
   dotspacemacs-colorize-cursor-according-to-state t

   ;; Default font, or prioritized list of fonts. `powerline-scale' allows to
   ;; quickly tweak the mode-line size to make separators look not too crappy.
   dotspacemacs-default-font '(("Menlo"
                                :size 13
                                :weight normal
                                :width normal)
                               ("DejaVu Sans Mono"
                                :size 14
                                :weight normal
                                :width normal)
                               ("Source Code Pro"
                                :size 13
                                :weight normal
                                :width normal)
                               ("Consolas"
                                :size 13
                                :powerline-scale 1.1))

   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"

   ;; The key used for Emacs commands `M-x' (after pressing on the leader key).
   ;; (default "SPC")
   dotspacemacs-emacs-command-key "SPC"

   ;; The key used for Vim Ex commands (default ":")
   dotspacemacs-ex-command-key ":"

   ;; The leader key accessible in `emacs state' and `insert state'
   ;; (default "M-m")
   dotspacemacs-emacs-leader-key "M-m"

   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","

   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m" for terminal mode, "<M-return>" for GUI mode).
   ;; Thus M-RET should work as leader key in both GUI and terminal modes.
   ;; C-M-m also should work in terminal mode, but not in GUI mode.
   dotspacemacs-major-mode-emacs-leader-key (if window-system "<M-return>" "C-M-m")

   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil

   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"

   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil

   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil

   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil

   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1

   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache

   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5

   ;; If non-nil, the paste transient-state is enabled. While enabled, after you
   ;; paste something, pressing `C-j' and `C-k' several times cycles through the
   ;; elements in the `kill-ring'. (default nil)
   dotspacemacs-enable-paste-transient-state nil

   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4

   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom

   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil

   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t

   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil

   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil

   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (default nil) (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil

   ;; If non-nil the frame is undecorated when Emacs starts up. Combine this
   ;; variable with `dotspacemacs-maximized-at-startup' in OSX to obtain
   ;; borderless fullscreen. (default nil)
   dotspacemacs-undecorated-at-startup nil

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-active-transparency 90

   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'. (default 90)
   dotspacemacs-inactive-transparency 90

   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t

   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t

   ;; If non-nil unicode symbols are displayed in the mode line.
   ;; If you use Emacs as a daemon and wants unicode characters only in GUI set
   ;; the value to quoted `display-graphic-p'. (default t)
   dotspacemacs-mode-line-unicode-symbols t

   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t

   ;; Show the scroll bar while scrolling. The auto hide time can be configured
   ;; by setting this variable to a number. (default t)
   dotspacemacs-scroll-bar-while-scrolling t

   ;; Control line numbers activation.
   ;; If set to `t', `relative' or `visual' then line numbers are enabled in all
   ;; `prog-mode' and `text-mode' derivatives. If set to `relative', line
   ;; numbers are relative. If set to `visual', line numbers are also relative,
   ;; but only visual lines are counted. For example, folded lines will not be
   ;; counted and wrapped lines are counted as multiple lines.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :visual nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; When used in a plist, `visual' takes precedence over `relative'.
   ;; (default nil)
   dotspacemacs-line-numbers nil

   ;; Code folding method. Possible values are `evil', `origami' and `vimish'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil

   ;; If non-nil and `dotspacemacs-activate-smartparens-mode' is also non-nil,
   ;; `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil

   ;; If non-nil smartparens-mode will be enabled in programming modes.
   ;; (default t)
   dotspacemacs-activate-smartparens-mode t

   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc...
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil

   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all

   ;; If non-nil, start an Emacs server if one is not already running.
   ;; (default nil)
   dotspacemacs-enable-server nil

   ;; Set the emacs server socket location.
   ;; If nil, uses whatever the Emacs default is, otherwise a directory path
   ;; like \"~/.emacs.d/server\". It has no effect if
   ;; `dotspacemacs-enable-server' is nil.
   ;; (default nil)
   dotspacemacs-server-socket-dir nil

   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil

   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")

   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; If nil then Spacemacs uses default `frame-title-format' to avoid
   ;; performance issues, instead of calculating the frame title by
   ;; `spacemacs/title-prepare' all the time.
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"

   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil

   ;; Show trailing whitespace (default t)
   dotspacemacs-show-trailing-whitespace t

   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'changed

   ;; If non-nil activate `clean-aindent-mode' which tries to correct
   ;; virtual indentation of simple modes. This can interfere with mode specific
   ;; indent handling like has been reported for `go-mode'.
   ;; If it does deactivate it here.
   ;; (default t)
   dotspacemacs-use-clean-aindent-mode t

   ;; Accept SPC as y for prompts if non-nil. (default nil)
   dotspacemacs-use-SPC-as-y nil

   ;; If non-nil shift your number row to match the entered keyboard layout
   ;; (only in insert state). Currently supported keyboard layouts are:
   ;; `qwerty-us', `qwertz-de' and `querty-ca-fr'.
   ;; New layouts can be added in `spacemacs-editing' layer.
   ;; (default nil)
   dotspacemacs-swap-number-row nil

   ;; Either nil or a number of seconds. If non-nil zone out after the specified
   ;; number of seconds. (default nil)
   dotspacemacs-zone-out-when-idle nil

   ;; Run `spacemacs/prettify-org-buffer' when
   ;; visiting README.org files of Spacemacs.
   ;; (default nil)
   dotspacemacs-pretty-docs nil

   ;; If nil the home buffer shows the full path of agenda items
   ;; and todos. If non-nil only the file name is shown.
   dotspacemacs-home-shorten-agenda-source nil

   ;; If non-nil then byte-compile some of Spacemacs files.
   dotspacemacs-byte-compile nil))

(defun dotspacemacs/user-env ()
  "Environment variables setup.
This function defines the environment variables for your Emacs session. By
default it calls `spacemacs/load-spacemacs-env' which loads the environment
variables declared in `~/.spacemacs.env' or `~/.spacemacs.d/.spacemacs.env'.
See the header of this file for more information."
  (spacemacs/load-spacemacs-env))

(defun dotspacemacs/user-init ()
  "Initialization for user code:
This function is called immediately after `dotspacemacs/init', before layer
configuration.
It is mostly for variables that should be set before packages are loaded.
If you are unsure, try setting them in `dotspacemacs/user-config' first."
  (defconst macos-p (string-equal system-type "darwin"))
  (defconst windows-p (string-equal system-type "windows-nt"))
  (defconst linux-p (string-equal system-type "gnu/linux"))

  (setq mac-command-modifier 'meta)
  (setq mac-option-modifier 'super)

  ;; custom-variables
  (setq custom-file "~/.emacs.d/.cache/custom-variables")
  (unless (file-exists-p custom-file)

    (write-region "" nil custom-file))
  (load custom-file)

  ;; tramp
  ;; (setq tramp-ssh-controlmaster-options
  ;;     "-o ControlMaster=auto -o ControlPath='tramp.%%C' -o ControlPersist=no -t")

  (setq nord-comment-brightness 20)
  (setq password-cache-expiry nil)
  (setq smerge-command-prefix "\C-cv")
  (eval-after-load 'helm-grep
    '(setq helm-grep-default-command helm-grep-default-recurse-command))

  (global-set-key (kbd "C-x g") 'magit-status)

  (add-to-list 'auto-mode-alist '("\\.md\\'" . markdown-mode))
  (setq ghub-use-workaround-for-emacs-bug nil)
)

(defun dotspacemacs/user-load ()
  "Library to load while dumping.
This function is called only while dumping Spacemacs configuration. You can
`require' or `load' the libraries of your choice that will be included in the
dump."
  )

(defun dotspacemacs/user-config ()
  "Configuration for user code:
This function is called at the very end of Spacemacs startup, after layer
configuration.
Put your configuration code here, except for variables that should be set
before packages are loaded."

  (defconst macos-p (string-equal system-type "darwin"))
  (defconst windows-p (string-equal system-type "windows-nt"))
  (defconst linux-p (string-equal system-type "gnu/linux"))

  ;; auto-newline
  (remove-hook 'c-mode-common-hook 'spacemacs//c-toggle-auto-newline)

  ;; auto-revert-mode
  (global-auto-revert-mode t)

  ;; clang-format
  ;; Bind clang-format-region to C-M-tab in all modes:
  (global-set-key (kbd "C-c f") 'clang-format-region)

  ;; ;; Can't remember why this was set, but it conflicts with Tramp so I'm
  ;; ;; killing it now
  ;;
  ;; (if windows-p (setq default-process-coding-system '(undecided-dos .
  ;;     undecided-dos)))

  ;; Bind clang-format-buffer to tab on the c++-mode only:
  (add-hook 'c++-mode-hook 'clang-format-c++-bindings)
  (defun clang-format-c++-bindings ()
    (define-key c++-mode-map [C-tab] 'clang-format-buffer))

  ;; Bind clang-format-buffer to tab on the c-mode only:
  (add-hook 'c-mode-hook 'clang-format-c-bindings)
  (defun clang-format-c-bindings ()
    (define-key c-mode-map [C-tab] 'clang-format-buffer))

  ;; calc shortcuts
  (spacemacs/set-leader-keys "cg" 'calc-grab-region)

  ;; counsel-mark-ring
  (spacemacs/set-leader-keys "rm" 'counsel-mark-ring)

  ;; company
  (with-eval-after-load 'company
    (define-key company-active-map (kbd "M-n") #'company-select-next)
    (define-key company-active-map (kbd "M-p") #'company-select-previous)
    (define-key company-active-map (kbd "C-n") nil)
    (define-key company-active-map (kbd "C-p") nil)
    (define-key company-active-map (kbd "C-f") nil)
    (define-key company-active-map (kbd "<tab>") nil)
    (define-key company-active-map (kbd "TAB") 'company-complete-selection)
    (define-key company-active-map (kbd "RET") nil)
    (define-key company-active-map (kbd "<return>") nil))

  ;; compile
  ;;
  ;; Adds support for VS2019 error messages
  (with-eval-after-load 'compile
    (add-to-list 'compilation-error-regexp-alist
                 '("\\(.*\\)(\\([0-9]+\\),\\([0-9]+\\)): error" 1 2 3)))

  ;; csharp
  (add-to-list 'auto-mode-alist '("\\.cs\\'" . csharp-mode))

  ;; CUDA
  (add-to-list 'auto-mode-alist '("\\.cu\\'" . c++-mode))

  ;; delete-selection-mode
  (delete-selection-mode 1)

  ;; flycheck
  (with-eval-after-load 'flycheck
    (flycheck-add-mode 'javascript-eslint 'web-mode)
    (setq-default flycheck-disabled-checkers '(c/c++-clang
                                               c/c++-gcc)))
  ;; hide-show
  (add-hook 'c-mode-common-hook
            (lambda()
              (local-set-key (kbd "C-<right>") 'hs-show-block)
              (local-set-key (kbd "C-<left>")  'hs-hide-block)
              (local-set-key (kbd "C-<up>")    'hs-hide-all)
              (local-set-key (kbd "C-<down>")  'hs-show-all)))

  ;; hungry delete
  (add-hook 'prog-mode-hook (lambda () (turn-on-hungry-delete-mode)))

  ;; dart
  (setq dart-enable-analysis-server t)
  (add-hook 'dart-mode-hook 'flycheck-mode)
  (global-set-key (kbd "C-c g h") (lambda () (interactive)
                                    (pomdart/component-open "html")))
  (global-set-key (kbd "C-c g d") (lambda () (interactive)
                                    (pomdart/component-open "dart")))
  (global-set-key (kbd "C-c g c") (lambda () (interactive)
                                    (pomdart/component-open "css")))
  (global-set-key (kbd "C-c g s") (lambda () (interactive)
                                    (pomdart/component-open "scss")))

  ;; dash
  (if macos-p
      (global-set-key (kbd "C-c d") 'dash-at-point)
    (global-set-key (kbd "C-c d") 'zeal-at-point))


  ;; default-directory
  (setq default-directory "~/")


  ;; dired stuff
  (setq dired-dwim-target t)
  (add-hook 'dired-mode-hook
            (lambda ()
              (dired-hide-details-mode)))

  ;; emmet
  (add-hook 'emmet-mode-hook
            (lambda ()
              ;; clearing old C-j and C-return key mapping.
              (define-key emmet-mode-keymap (kbd "C-j")        nil)
              (define-key emmet-mode-keymap (kbd "<C-return>") nil)
              ;; define new M-j key mapping instead.
              (define-key emmet-mode-keymap (kbd "M-j")        'emmet-expand-line)))

  ;; Evil
  (add-hook 'prog-mode-hook #'(lambda () (modify-syntax-entry ?_ "w")))

  ;; Help
  (global-set-key (kbd "C-c h") 'open-help)



  ;; ffap
  (global-set-key (kbd "C-,") 'ffap)

  ;; forward-to-word
  (global-set-key (kbd "M-F") 'forward-to-word)

  ;; frame-cmds
  (require 'frame-cmds)
  (require 'hydra)
  (defhydra hydra-zoom (global-map "<f2>")
    "Resize frame"
    ("j" enlarge-frame "V enlarge")
    ("k" shrink-frame "V shrink")
    ("h" shrink-frame-horizontally "H shrink")
    ("l" enlarge-frame-horizontally "H enlarge")
    ("C-j" move-frame-down "Move down")
    ("C-k" move-frame-up "Move up")
    ("C-h" move-frame-left "Move left")
    ("C-l" move-frame-right "Move right"))

  ;; Make find-dired's output work on Windows
  (if windows-p
      (setq find-ls-option
            '("-printf \"%M %d %u %g %s %AY-%Am-%Ad %p\\n\" " . "-dilsb")))

  ;; Frame size
  (add-to-list 'default-frame-alist '(width . 83))

  ;; gravatar
  (setq magit-revision-show-gravatars nil)


  ;; iso-transl
  ;; This seems to be required to prevent "<dead-acute> is undefined" errors
  (require 'iso-transl)

  ;; ivy
  (ivy-add-actions
   'counsel-find-file
   '(("x" counsel-find-file-extern "open externally")))
  (setq ivy-dynamic-exhibit-delay-ms 200)

  ;; js2-mode
  (setq js2-strict-missing-semi-warning nil)
  (setq js2-missing-semi-one-line-override nil)

  ;; large-file-mode
  (setq spacemacs-large-file-modes-list
   (quote
    (archive-mode tar-mode jka-compr git-commit-mode image-mode doc-view-mode
                  doc-view-mode-maybe ebrowse-tree-mode pdf-view-mode tags-table-mode)))

  ;; lsp-mode
  (require 'lsp)
  (add-to-list 'lsp-file-watch-ignored '"[/\\\\]build$")
  ;; (add-hook 'c++-mode-hook #'lsp)
  ;; (add-hook 'c-mode-hook #'lsp)
  ;; (setq lsp-client-packages '(lsp-clients lsp-dart lsp-json lsp-perl lsp-pyls
  ;;                                         lsp-python-ms lsp-rust lsp-verilog
  ;;                                         lsp-vhdl lsp-xml lsp-yaml))

  ; See https://github.com/emacs-lsp/lsp-mode/issues/713
  (defun ++git-ignore-p (path)
    (let* (; trailing / breaks git check-ignore if path is a symlink:
           (path (directory-file-name path))
           (default-directory (file-name-directory path))
           (relpath (file-name-nondirectory path))
           (cmd (format "git check-ignore '%s'" relpath))
           (status (call-process-shell-command cmd)))
      (eq status 0)))

  (defun ++lsp--path-is-watchable-directory-a
      (fn path dir ignored-directories)
    (and (not (++git-ignore-p (f-join dir path)))
         (funcall fn path dir ignored-directories)))

  (advice-add 'lsp--path-is-watchable-directory
              :around #'++lsp--path-is-watchable-directory-a)

  ;; Move Where I Mean
  (global-set-key (kbd "C-a") 'mwim-beginning)
  (global-set-key (kbd "C-e") 'mwim-end)

  ;; octave-mode
  (setq auto-mode-alist
        (cons '("\\.m$" . octave-mode) auto-mode-alist))
  (setq octave-block-offset 4)

  ;; org-babel
  (setq org-babel-sh-command "bash")
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (shell . t)
     (ditaa . t)
     (python . t)
     (plantuml . t)
     (C . t)))

  (add-hook 'org-babel-after-execute-hook 'org-redisplay-inline-images)

  ;; org-capture
  (setq org-default-notes-file (expand-file-name "~/Dropbox/notes/notes.org"))
  (define-key global-map "\C-cc" 'org-capture)

  (require 'org-chef)
  (setq org-capture-templates
        '(("c" "Cookbook" entry (file "~/Dropbox/org/cookbook.org")
           "%(org-chef-get-recipe-from-url)"
           :empty-lines 1)
          ("m" "Manual Cookbook" entry (file "~/Dropbox/org/cookbook.org")
           "* %^{Recipe title: }\n  :PROPERTIES:\n  :source-url:\n  :servings:\n  :prep-time:\n  :cook-time:\n  :ready-in:\n  :END:\n** Ingredients\n   %?\n** Directions\n\n")))

  ;; org-indent
  (with-eval-after-load 'org (setq org-startup-indented nil))

  ;; org-mode
  (eval-after-load "org"
    '(progn
       ;; .txt files aren't in the list initially, but in case that changes
       ;; in a future version of org, use if to avoid errors
       (if (assoc "\\.png\\'" org-file-apps)
           (setcdr (assoc "\\.png\\'" org-file-apps) "mypaint %s")
         (add-to-list 'org-file-apps '("\\.png\\'" . "mypaint %s") t))))


  ;; origami
  (global-set-key (kbd "C-c o") 'origami-recursively-toggle-node)

  ;; paradox
  (setq paradox-github-token "f017f2e09dfbc2960d6890bb85675d98fb25618f")

  ;; plantuml
  (setq org-plantuml-jar-path
        (expand-file-name "~/.spacemacs.d/res/plantuml.jar"))

  ;; projectile
  (setq projectile-switch-project-action 'projectile-vc)
  (setq projectile-keymap-prefix (kbd "C-x p"))
  (setq projectile-enable-caching t)
  (if windows-p
      (setq projectile-indexing-method 'alien))
  (global-set-key (kbd "M-m m g a") 'projectile-find-other-file)

  ;; python
  (setenv "PYTHONIOENCODING" "utf-8") ; See https://emacs.stackexchange.com/a/31283

  ;; rust
  (setq-default dotspacemacs-configuration-layers
                '(lsp :variables lsp-rust-server 'rust-analyzer))

  ;; macos keys
  (if macos-p
      (setq mac-right-option-modifier 'nil))

  ;; mouse
  (setq mouse-wheel-progressive-speed nil)
  (if macos-p
      (setenv "PATH" (concat "/usr/local/opt/python/libexec/bin:"
                             (getenv "PATH"))))
  (add-hook 'spacemacs-buffer-mode-hook
            (lambda ()
              (set (make-local-variable 'mouse-1-click-follows-link) nil)))

  ;; magit
  ; Selects buffer for new magit windows. Was set to
  ; #'magit-display-buffer-same-window-except-diff-v1
  (setq magit-display-buffer-function
        (lambda (buffer)
          (display-buffer buffer '(display-buffer-same-window))))
  (with-eval-after-load 'magit
    (remove-hook 'server-switch-hook 'magit-commit-diff)
    (if windows-p
        (setq magit-git-executable "c:/Users/rdeterre/scoop/shims/git.exe")
      (setq magit-git-executable "git"))
    (setq magit-refresh-status-buffer nil)
    (setq magit-verbose-refresh t))

  ;; moom
  (with-eval-after-load "moom"
    ;; add settings here ...
    (moom-mode 1))

  ;; multiple-cursors
  (with-eval-after-load 'multiple-cursors
    (global-set-key (kbd "C-S-c C-S-c") 'mc/edit-lines)
    (global-set-key (kbd "C->") 'mc/mark-next-like-this)
    (global-set-key (kbd "C-<") 'mc/mark-previous-like-this)
    (global-set-key (kbd "C-c C-<") 'mc/mark-all-like-this)
    (global-set-key (kbd "C-c i n") 'mc/insert-numbers)
    (global-set-key (kbd "C-c i l") 'mc/insert-letters))
  (require 'multiple-cursors)

  ;; golden-ratio-scroll-screen
  (require 'golden-ratio-scroll-screen)
  (global-set-key [remap scroll-down-command] 'golden-ratio-scroll-screen-down)
  (global-set-key [remap scroll-up-command] 'golden-ratio-scroll-screen-up)

  ;; operate-on-number
  (global-set-key (kbd "C-.") 'operate-on-number-at-point)

  ;; prettier-js
  (require 'prettier-js)
  (add-hook 'web-mode-hook #'(lambda ()
                               (enable-minor-mode
                                '("\\.jsx?\\'" . prettier-js-mode))))
  (add-hook 'web-mode-hook #'(lambda ()
                               (enable-minor-mode
                                '("\\.vue\\'" . prettier-js-mode))))

  ;; python3 by default
  (setq py-python-command "python3")
  (setq python-shell-interpreter "python3")
  (setq flycheck-python-pycompile-executable "python3")
  (setq flycheck-python-flake8-executable "python3")
  (setq flycheck-python-pylint-executable "python3")

  ;; ripgrep
  (require 'rg)
  (global-set-key (kbd "M-s") 'rg-dwim)
  (global-set-key (kbd "M-S") 'rg)
  (setq rg-custom-type-aliases
        '(("rc" .    "*.rc")
          ("clike" . "*.c *.h *.cpp *.hpp")))

  (setq rg-command-line-flags '("--no-ignore"))

  ;; scroll
  (setq scroll-conservatively 101
        scroll-margin 0
        scroll-preserve-screen-position 'nil)

  ;; serial-term
  (defun replace-all (string to-find to-replace)
    (let ((index  (cl-search to-find string))
          (pos    0)
          (result ""))
      (while index
        (setq result (concat result
                             (substring string pos index)
                             to-replace)
              pos    (+ index (length to-find))
              index  (cl-search to-find string :start2 pos)))
      (concat result (substring string pos))))

  (require 'term)
  (defun rephone-serial-process-filter (process output)
    "Replace LF in output string with CR+LF."
    (term-emulate-terminal process
                           (replace-all output
                                        (byte-to-string ?\n)
                                        (string ?\r ?\n))))

  (defun rephone-serial-term (port)
    "Basically duplicate SERIAL-TERM from term.el but with process
  filtering to translate LF to CR+LF."
    (interactive (list (serial-read-name)))
    (serial-supported-or-barf)
    (let* ((process (make-serial-process
                     :port port
                     :speed 115200
                     :bytesize 8
                     :parity nil
                     :stopbits 1
                     :flowcontrol nil
                     :coding 'raw-text-unix
                     :noquery t
                     :name (format "Lua:%s" port)
                     :filter 'rephone-serial-process-filter
                     :sentinel 'term-sentinel))
           (buffer (process-buffer process)))
      (with-current-buffer buffer
        (term-mode)
        (term-line-mode)
        (goto-char (point-max))
        (set-marker (process-mark process) (point)))
      (switch-to-buffer buffer)
      buffer))

  ;; smart-beginning-of-line
  (global-set-key (kbd "C-S-a") 'smart-beginning-of-line)

  ;; smartparens
  (global-set-key (kbd "M-<delete>") 'sp-unwrap-sexp)

  (use-package smerge-mode
    :after hydra
    :config
    (defhydra unpackaged/smerge-hydra
      (:color pink :hint nil :post (smerge-auto-leave))
      "
^Move^       ^Keep^               ^Diff^                 ^Other^
^^-----------^^-------------------^^---------------------^^-------
_n_ext       _b_ase               _<_: upper/base        _C_ombine
_p_rev       _u_pper              _=_: upper/lower       _r_esolve
^^           _l_ower              _>_: base/lower        _k_ill current
^^           _a_ll                _R_efine
^^           _RET_: current       _E_diff
"
      ("n" smerge-next)
      ("p" smerge-prev)
      ("b" smerge-keep-base)
      ("u" smerge-keep-upper)
      ("l" smerge-keep-lower)
      ("a" smerge-keep-all)
      ("RET" smerge-keep-current)
      ("\C-m" smerge-keep-current)
      ("<" smerge-diff-base-upper)
      ("=" smerge-diff-upper-lower)
      (">" smerge-diff-base-lower)
      ("R" smerge-refine)
      ("E" smerge-ediff)
      ("C" smerge-combine-with-next)
      ("r" smerge-resolve)
      ("k" smerge-kill-current)
      ("ZZ" (lambda ()
              (interactive)
              (save-buffer)
              (bury-buffer))
       "Save and bury buffer" :color blue)
      ("q" nil "cancel" :color blue))
    :hook (magit-diff-visit-file . (lambda ()
                                     (when smerge-mode
                                       (unpackaged/smerge-hydra/body)))))

  ;; ;; spaceline customization
  ;; ;;
  ;; ;; the goal here is to make the buffer-id (buffer name) stay as long as
  ;; ;; possible when the window shrinks.
  ;; ;;
  ;; ;; https://github.com/TheBB/spaceline/issues/147#issuecomment-345047554
  ;; (require 'spaceline-config)
  ;; (apply 'spaceline--theme
  ;;        '((persp-name
  ;;           workspace-number
  ;;           window-number)
  ;;          :fallback evil-state
  ;;          :face highlight-face
  ;;          :priority 0)
  ;;        '((buffer-id)
  ;;          :priority -10)
  ;;        '((buffer-modified buffer-size)
  ;;          :priority 5)
  ;;        '((remote-host)
  ;;          :priority 5))
  ;; spaceline
  (spaceline-toggle-minor-modes-off)
  (spaceline-toggle-version-control-off)
  (spaceline-toggle-buffer-encoding-off)
  (spaceline-toggle-buffer-encoding-abbrev-off)
  (spaceline-toggle-purpose-off)

  ;; tags
  ;;
  ;; See https://emacs.stackexchange.com/a/14803/10314
  (setq tags-add-tables nil)

  ;; todolist
  (global-set-key (kbd "C-c t") 'todolist)

  ;; useful buffers
  (push "\\*magit" spacemacs-useful-buffers-regexp)
  (push "\\*Find" spacemacs-useful-buffers-regexp)

  ;; yasnippet
  (with-eval-after-load 'yasnippet
    (yas-global-mode 1))


  ;; ;; ycmd
  ;; (if windows-p
  ;;     (set-variable 'ycmd-server-command
  ;;                   '("python" "C:\Users\RDeterre\Documents\ycmd")))

  ;; vc-mode
  (setq vc-handled-backends nil)

  ;; vue
  (add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))

  ;; ;; web-mode
  ;; (add-to-list 'auto-mode-alist '("\\.vue\\'" . web-mode))
  ;; (add-to-list 'auto-mode-alist '("\\.js\\'" . web-mode))
  ;; (setq web-mode-markup-indent-offset 2)
  ;; (setq web-mode-code-indent-offset 2)
  ;; (setq web-mode-enable-auto-quoting nil)
  ;; (add-hook 'web-mode-hook
  ;;           (lambda ()
  ;;             (if (equal web-mode-content-type "javascript")
  ;;                 (web-mode-set-content-type "jsx"))))

  ;; word manipulation
  (global-set-key (kbd "C-c v s") 'word-or-region-to-snake)

  ;; world clock
  (setq display-time-world-list '(("PST8PDT" "San Francisco")
                                  ("EST5EDT" "Montreal")
                                  ("CET-1CDT" "Paris")
                                  ("CST-8" "Shanghai")
                                  ("JST-9" "Tokyo")))

  ;; Bug workaround
  ;;
  ;; https://debbugs.gnu.org/cgi/bugreport.cgi?bug=31586
  (setq frame-title-format nil)
  )

;; Utility functions

(require 'ediff)

(defun fdiff (buffer-A buffer-B &optional startup-hooks)
  "Run Ediff on a pair of function in specified buffers.
Regions (i.e., point and mark) can be set in advance or marked interactively.
This function is effective only for relatively small regions, up to 200
lines.  For large regions, use `ediff-regions-linewise'."
  (interactive
   (let (bf)
     (list (setq bf (read-buffer "Region's A buffer: "
				 (ediff-other-buffer "") t))
	   (read-buffer "Region's B buffer: "
			(progn
			  ;; realign buffers so that two visible bufs will be
			  ;; at the top
			  (save-window-excursion (other-window 1))
			  (ediff-other-buffer bf))
			t))))
  (if (not (ediff-buffer-live-p buffer-A))
      (error "Buffer %S doesn't exist" buffer-A))
  (if (not (ediff-buffer-live-p buffer-B))
      (error "Buffer %S doesn't exist" buffer-B))


  (let (reg-A reg-A-beg reg-A-end reg-B reg-B-beg reg-B-end)
    (with-current-buffer buffer-A
      (setq reg-A (bounds-of-thing-at-point 'defun))
      (setq reg-A-beg (car reg-A))
      (setq reg-A-end (cdr reg-A))
      (set-buffer buffer-B)
      (setq reg-B (bounds-of-thing-at-point 'defun))
      (setq reg-B-beg (car reg-B))
      (setq reg-B-end (cdr reg-B)))
    (ediff-regions-internal
     (get-buffer buffer-A) reg-A-beg reg-A-end
     (get-buffer buffer-B) reg-B-beg reg-B-end
     startup-hooks 'ediff-regions-linewise 'word-mode nil)))

(defun incalc (start end)
  "Replaces the content of the area with the result of the calc expression"
  (interactive "r")
  (let ((result (calc-eval (buffer-substring start end))))
    (delete-region start end)
    (insert result)))

(defun smart-beginning-of-line ()
  "Move point to first non-whitespace character or beginning-of-line.

Move point to the first non-whitespace character on this line.
If point was already at that position, move point to beginning of line."
  (interactive)
  (let ((oldpos (point)))
    (back-to-indentation)
    (and (= oldpos (point))
         (beginning-of-line))))

(defun todolist ()
  "Lists all the TODO keywords in the project"
  (interactive)
  (rg-project "TODO" "all"))

(defun open-help ()
  (interactive)
  (find-file "p:/Claude Henry/emacs.org"))

(defun enable-minor-mode (my-pair)
  "Enable minor mode if filename match the regexp.  MY-PAIR is a cons cell (regexp . minor-mode)."
  (if (buffer-file-name)
      (if (string-match (car my-pair) buffer-file-name)
          (funcall (cdr my-pair)))))

(defun kill-other-buffers ()
  "Kill all other buffers."
  (interactive)
  (mapc 'kill-buffer
        (delq (current-buffer)
              (remove-if-not 'buffer-file-name (buffer-list)))))

;;
;; Emacs Lisp Cheat Sheet
;;

;; Generate a sequence
;;
;; (dotimes (i 20) (insert (format "%2d.\n" (1+ i))))

(defun projectile-default-generic-command (project-type command-type)
  "Generic retrieval of COMMAND-TYPEs default cmd-value for PROJECT-TYPE.

If found, checks if value is symbol or string. In case of symbol resolves
to function `funcall's. Return value of function MUST be string to be executed as command."
  (let ((command (plist-get (gethash project-type projectile-project-types) command-type)))
    (cond
     ((stringp command) command)
     ((functionp command)
      (if (fboundp command)
          (funcall (symbol-function command)))))))

(defun find-first-non-ascii-char ()
  "Find the first non-ascii character from point onwards."
  (interactive)
  (let (point)
    (save-excursion
      (setq point
            (catch 'non-ascii
              (while (not (eobp))
                (or (eq (char-charset (following-char))
                        'ascii)
                    (throw 'non-ascii (point)))
                (forward-char 1)))))
    (if point
        (goto-char point)
      (message "No non-ascii characters."))))

(defun pomdart/component-open (extension)
  "Opens the HTML file associated with the currently opened component"
  (interactive)
  (find-file (concat (file-name-sans-extension (buffer-name))
                     "."
                     extension)))

(defun word-or-region-to-snake ()
  "Convert word at point (or selected region) to snake case."
  (interactive)
  (let* ((bounds (if (use-region-p)
                     (cons (region-beginning) (region-end))
                   (bounds-of-thing-at-point 'symbol)))
         (text   (buffer-substring-no-properties (car bounds) (cdr bounds))))
    (when bounds
      (delete-region (car bounds) (cdr bounds))
      (insert (s-snake-case text)))))

(defun win-maxh ()
  (interactive)
  (require 'moom)
  (moom-move-frame-to-edge-top)
  (moom-change-frame-height 1000))

(defun win-singlew ()
  (interactive)
  (require 'moom)
  (moom-change-frame-width '86))

(defun win-doublew ()
  (interactive)
  (require 'moom)
  (moom-change-frame-width '172))

(defun swap-meta-and-super ()
  "Swap the mapping of meta and super. Very useful for people using their Mac
with a Windows external keyboard from time to time."
  (interactive)
  (if (eq mac-command-modifier 'super)
      (progn
        (setq mac-command-modifier 'meta)
        (setq mac-option-modifier 'super)
        (message "Command is now bound to META and Option is bound to SUPER."))
    (progn
      (setq mac-command-modifier 'super)
      (setq mac-option-modifier 'meta)
      (message "Command is now bound to SUPER and Option is bound to META."))))
